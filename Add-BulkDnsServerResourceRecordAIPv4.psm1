﻿<#

.SYNOPSIS

Bulk inserts DNS records from CSV file

.DESCRIPTION

Adds A records with IPV4 addresses from CSV file. 
CSV file must contain Name and IPv4 headers.

.PARAMETER CSVFile 

Path to CSV file


.PARAMETER Delimiter

CSV file delimiter

.PARAMETER ZoneName

DNS zone name where you want to create A records

.PARAMETER Computername

DNS server hostname or IP address

.PARAMETER CreatePtr

If this switch used,  PTR record will be created


.EXAMPLE

Add-BulkDnsServerResourceRecordAIPv4 -CSVFile C:\temp\network_equipement.csv -Delimiter ',' -ZoneName domain.com -Computername DNSServer1 -CreatePtr


.NOTES

Module DnsServer must be present otherwise function will not work.
Tested with PS v5 only.

#>
Function Add-BulkDnsServerResourceRecordAIPv4 {
[CmdletBinding()]
    Param ( [Parameter(Mandatory=$true)]
            [string]$CSVFile,
            [Parameter(Mandatory=$true)]
            [string]$Delimiter,
            [Parameter(Mandatory=$true)]
            [string]$ZoneName,
            [Parameter(Mandatory=$true)]
            [string]$Computername,
            [switch]$CreatePtr
            )

    try {
        Import-Module DnsServer -ErrorAction Stop
    } catch {
        Write-Output "Missing module DnsServer"
        Exit 1
    }
    $IPv4Regex = '^(?:(?:0?0?\d|0?[1-9]\d|1\d\d|2[0-5][0-5]|2[0-4]\d)\.){3}(?:0?0?\d|0?[1-9]\d|1\d\d|2[0-5][0-5]|2[0-4]\d)$'
    $records = Import-Csv $CSVFile -Delimiter $Delimiter
    if (!($records | Get-Member -MemberType NoteProperty -Name Name))
    { 
        throw "InputObject does not have Name NoteProperty" 
        Exit 1
    }
    if (!($records | Get-Member -MemberType NoteProperty -Name IPv4))
    { 
        throw "InputObject does not have IPv4 NoteProperty" 
        Exit 1
    }

    Write-Output "Testing DNS Server"
    if (Test-DnsServer -IPAddress $Computername -ComputerName $Computername)
    {

        foreach ($record in $records)
        {
            if (([regex]::Matches($($record.ipv4), $IPv4Regex)).Success)
            {

                $getRecord = $null
                $getRecord = Get-DnsServerResourceRecord -ComputerName $Computername `
                                                         -ZoneName $ZoneName `
                                                         -Name $record.name `
                                                         -RRType A `
                                                         -ErrorAction SilentlyContinue

                if ([String]::IsNullOrEmpty($getRecord))
                {      
                    try {
                        Write-Output "Adding Record $($record.name)"
                        if ($CreatePtr)
                        {
                            Add-DnsServerResourceRecordA -ZoneName $ZoneName `
                                                         -Name $record.name `
                                                         -IPv4Address $record.ipv4 `
                                                         -CreatePtr `
                                                         -ComputerName $Computername `
                                                         -ErrorAction Stop
                        }
                        else
                        {
                            Add-DnsServerResourceRecordA -ZoneName $ZoneName `
                                                         -Name $record.name `
                                                         -IPv4Address $record.ipv4 `
                                                         -ComputerName $Computername `
                                                         -ErrorAction Stop
                        }

                    } catch {
                        Write-Output $Error[0].Exception.Message
                    }
                }
                else
                {
                    Write-Output "Record $($record.name) already exist"
                }
            }
            else
            {
                Write-Output "$($record.ipv4) is not valid IPv4 address"
            }
        }
    }
    else
    {
        Write-Output "Failed to connect to DNS server"
        Exit 1
    }
}
