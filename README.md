
```
#!powershell

<#

.SYNOPSIS

Bulk inserts DNS records from CSV file

.DESCRIPTION

Adds A records with IPV4 addresses from CSV file. 
CSV file must contain Name and IPv4 headers.

.PARAMETER CSVFile 

Path to CSV file


.PARAMETER Delimiter

CSV file delimiter

.PARAMETER ZoneName

DNS zone name where you want to create A records

.PARAMETER Computername

DNS server hostname or IP address

.PARAMETER CreatePtr

If this switch used,  PTR record will be created


.EXAMPLE

Add-BulkDnsServerResourceRecordAIPv4 -CSVFile C:\temp\network_equipement.csv -Delimiter ',' -ZoneName domain.com -Computername DNSServer1 -CreatePtr


.NOTES

Module DnsServer must be present otherwise function will not work.
Tested with PS v5 only.

#>
```